import { HeroService } from './../hero.service';
import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  heroes: Hero[]=[];
  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    this.getHeross();
  }
  getHeros(){
    this.heroService.getHeroes()
    .subscribe(data=>this.heroes = data.slice(1,5));
  }

getHeross(){
  this.heroService.getHeross()
  .subscribe(data=> this.heroes = data.slice(0,5));
}
}
