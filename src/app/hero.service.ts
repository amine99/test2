import { MessageService } from './message.service';
import { HEROES } from './mock-heroes';
import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private log(message: string) {
    this.messageService.addMessage(`HeroService: ${message}`);
  }
  private baseUrl='http://localhost:8087/api/heros';
  constructor(private messageService:MessageService, private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getHeroes(): Observable<Hero[]>{
    const heroes= of (HEROES);
    this.messageService.addMessage('heroservice: fetched heroes');
    return heroes;
  }
  getHeroId(id:number): Observable<Hero>{
const hero = HEROES.find(h =>h.id === id)!;
this.messageService.addMessage(`HeroService: fetched hero id=${id}`);
    return of(hero);
  }
  getHeross():Observable<Hero[]>{
    return this.http.get<Hero[]>(this.baseUrl)
    .pipe(
      tap(_ =>this.log('fetshed heros')),
      catchError(this.handleError<Hero[]>('get heroes',[]))
    );
  }
  getHerosById(id: number): Observable<Hero>{
    this.messageService.addMessage(`HeroService: fetched hero id=${id}`);
    return this.http.get<Hero>(`${this.baseUrl}/${id}`).
    pipe(tap(_=>this.log(`fetched hero id=${id}`)),
    catchError(this.handleError<Hero>(`get hero id${id}`))
    );

  }
   updateHero(hero: Hero, id: number): Observable<any>{
    /* return this.http.put(`${this.baseUrl}/${id}`,hero, this.httpOptions).pipe(
       tap(_ =>this.log(`updated hero id:${id}`)),
       catchError(this.handleError<any>('update Hero'))
     );*/
     return this.http.put<Hero>(`${this.baseUrl}/${id}`,hero, this.httpOptions).pipe(
       tap(_=>this.log(`update hero id=${id}`)),
     catchError(this.handleError<any>('update hero'))
     );
   }
   addHero(hero: Hero): Observable<Hero>{
     return this.http.post<Hero>(this.baseUrl, hero, this.httpOptions)
     .pipe(
       tap((newHero: Hero) =>this.log(`add bnew hero id is :${newHero.id}`)),
       catchError(this.handleError<Hero>('addHero'))
     );
   }

   delHero(id: number): Observable<Hero>{
     return this.http.delete<Hero>(`${this.baseUrl}/${id}`, this.httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Hero>('deleteHero'))
    );
   }
   searchHero(term: string): Observable<Hero[]>{
     if(!term.trim()){
       return of([]);
     }
     return this.http.get<Hero[]>(`${this.baseUrl}/?nom=${term}`)
     .pipe(
       tap(data=>data.length ?this.log(`term serached found${term}`):this.log(`term serached not found${term}`)
       ),
       catchError(this.handleError<Hero[]>('search hero', []))
     );
   }






  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}


