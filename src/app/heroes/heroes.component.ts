import { MessageService } from './../message.service';
import { HeroService } from './../hero.service';
import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
heroes :Hero[]=[];

  constructor(private heroService:HeroService, private msgService: MessageService) { }

  ngOnInit(): void {
this.getHeros();
  }

  getHeros(){
   this.heroService.getHeross()
   .subscribe(data =>this.heroes = data);
    //.subscribe(data => this.heroes =data);
  }
  addHero(nom: string): void{
   nom=nom.trim();
   if(!nom){return ;}
   this.heroService.addHero({nom} as Hero)
   .subscribe(hero => {
     this.heroes.push(hero);
  });
  }
  deleteHero(hero: Hero):void{
    this.heroes= this.heroes.filter(h => h !== hero);
    this.heroService.delHero(hero.id).subscribe();
  }
}
