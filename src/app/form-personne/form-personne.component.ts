import { Personne } from './../personne';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-form-personne',
  templateUrl: './form-personne.component.html',
  styleUrls: ['./form-personne.component.css']
})
export class FormPersonneComponent implements OnInit {

  profils=['junior', 'senior', 'manager', 'secretaire'];
//person = new Personne(1,'','','');
//person?: Personne;
model=  new Personne(0,'','');
status= true;
color='green';

  submitted = false;
  constructor() { }

  ngOnInit(): void {
//this.newPerson();
  }

  onsubmit(){

    this.submitted = true;
  }

  newPerson(){
    this.submitted = true;
    console.log(this.submitted);
    this.model= new Personne(0,'','');
  }
  makeFalse(){
    this.status=false;
  }
  makeTrue(){
    this.status=true;
  }
  getColor(): string{
    return this.status===true?'green':'red';
  }

}
