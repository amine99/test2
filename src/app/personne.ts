export class Personne{

    constructor(
     public id: number,
     public nom: string,
     public profil: string,
     public adresse?: string
    ){

    }
}
