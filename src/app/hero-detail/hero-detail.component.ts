import { HeroService } from './../hero.service';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
   hero?: Hero;
  constructor(private route:ActivatedRoute,
    private heroServie: HeroService,
    private location: Location
    ) { }

  ngOnInit(): void {
    this.getHero();
  }

  getHero(): void{
   const id = Number(this.route.snapshot.paramMap.get('id'));
  // this.heroServie.getHeroId(id).subscribe(data => this.hero=data);
  this.heroServie.getHerosById(id).subscribe(data => this.hero= data);
  }
  goBack():void{
    this.location.back();
  }
  onSave():void{
    if(this.hero){
    this.heroServie.updateHero(this.hero, this.hero.id)
    .subscribe(()=> this.goBack());
    }
  }
}
